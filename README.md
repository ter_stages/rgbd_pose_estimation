# RGBD_Pose_Estimation_and_Evaluation



## Getting started

### Realsense RGBD Camera
[Realsense Python SDK](https://dev.intelrealsense.com/docs/python2)

`pip install pyrealsense2`

[Example of pyrealsense for L515](https://gricad-gitlab.univ-grenoble-alpes.fr/pacbot_group/vision_pacbot/-/blob/main/perception/src/perception/vision_driver.py#L52)

## RGBD Pose Estimation
[Open Pose](https://github.com/CMU-Perceptual-Computing-Lab/openpose)


## Pose Evaluation (from Ergonomic point of view)
[RULA Rapid Upper Limb Assesment](https://nawo-solution.com/rula-method)